from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Task
from .forms import TaskForm,TaskModelForm
from django.views.generic import TemplateView,View,ListView,DetailView
from django.views.generic.edit import FormView,CreateView,UpdateView,DeleteView
# Create your views here.


def index(request):
    tasks = Task.objects.all()
    form = TaskForm(initial={'text': 'text'})
    context = {
        'tasks': tasks,
        "form": form
    }
    return render(request,'myapp/index.html', context)
        # HttpResponse('hello django')


def create_task(request):
    if request.method == 'POST':
        # POST.getlist (is many elements with one name )
        form = TaskModelForm(request.POST)
        if form.is_valid():
            form.save()
            # Task.objects.create(
            #     text=form.cleaned_data['text'],
            #     checked=form.cleaned_data.get('checked', False)
            # )

        # task = Task(text=request.POST.get('text'), checked=bool(request.POST.get('checked',False)))
        # task.save()
    return redirect('/tasks')


def detail(request, pk):
    try:
        task = Task.objects.get(id=pk)
        if request.method == 'POST':
            form = TaskModelForm(request.POST, instance=task)
            if form.is_valid():
                form.save()
                return redirect('/tasks/{}'.format(task.pk))
            return render(request, 'myapp/detail.html', {"task": task,
                                                         "form": form})

        form = TaskModelForm(instance=task)
        context = {
            "task": task,
            "form": form

        }
        return render(request,'myapp/detail.html', context)
    except Task.DoesNotExist as e:
        print(e.message)


class TaskView(View):
    def get(self, request,*args, **kwargs):
        tasks = Task.objects.all()
        form = TaskForm(initial={'text': 'text'})
        context = {
            'tasks': tasks,
            "form": form
        }
        return render(request, 'myapp/index.html', context)

    def post(self,request, pk=None, *args, **kwargs):
        if request.method == 'POST':
            form = TaskModelForm(request.POST)
            if form.is_valid():
                form.save()

        return redirect('/tasks')


class TaskListView(ListView):
    model = Task
    paginate_by = 5
    context_object_name = 'tasks'
    queryset = Task.objects.all()

    # template_name =
    def get_context_data(self, *, object_list=None, **kwargs):
        data = super().get_context_data()
        data['form']=TaskForm()
        return data


class TaskDetailView(DetailView):
    model = Task


    def get_object(self, queryset=None):
        Task = super().get_queryset(self.request)


class TaskFormView(FormView):
    # model = Task
    form_class = TaskForm
    success_url = '/'

    def form_valid(self, form):
        pass




