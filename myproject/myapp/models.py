from django.db import models
import uuid

# Create your models here.


class MyModel(models.Model):
    pass


class Task(models.Model):
    # has default id (integer)

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    text = models.CharField(max_length=64)
    checked = models.BooleanField()
        # null=True,
        # blank=True,
        # default=False,
        # verbose_name="Check")
    # blank allows write to db empty value

    # class Meta:
    #     verbose_name='description of the model'
    #     verbose_name_plural = 'description of the model'
    #     unique_together = ['text', 'checked']
    #     help_text = 'using for help :)'
    #     # db_name = '' - sets inside field