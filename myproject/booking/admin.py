from django.contrib import admin
from booking.models import Author,Book, Tag


# Register your models here.


admin.site.register(Author)
admin.site.register(Book)
admin.site.register(Tag)
