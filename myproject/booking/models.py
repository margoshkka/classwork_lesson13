from django.db import models

# Create your models here.


class Author(models.Model):
    name = models.CharField(max_length=20)
    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name

class BookManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(author__isnull=False)



class Book(models.Model):
    title = models.CharField(max_length=64)
    author = models.ForeignKey(Author,
                               models.SET,
                               related_name='books'
                               )
    with_author = BookManager()
    # Book.with_author.all()
    tags = models.ManyToManyField(Tag)
    new_tags = models.ManyToManyField(Tag,
                                  through='BookTags',
                                      related_name='new_book_tags')

    def __str__(self):
        return self.title + ' ' + self.author.name


class BookTags(models.Model):
    book = models.ForeignKey(Book,
                             models.PROTECT)
    tag = models.ForeignKey(Tag,
                            models.PROTECT,
                            related_name='book_tags')

    def with_tag(self, tag):
        return self.get_queryset().filter(tags_name=tag)


